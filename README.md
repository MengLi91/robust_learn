# Basic information:
Author: Meng Li

Date: Jan 09, 2018

Function: Verify the impact of the variation for the weights and bias in the model

# How to run training?
```
python scripts/NNTop.py json/paramsTrain.json
```
In the default setting, in the training process, the default noise amplitude is 0.

# How to run testing?
```
python scripts/NNTop.py json/paramsTest.json
```

# Detail explanation for each file:
1. scripts/Params.py: define all the parameters
2. scripts/NNModel.py: define the model
3. scripts/NNTop.py: define the flow to train the model or use it for prediction

# How to add noise to model parameters (weight and bias):
Change the function "_gen_noise()" in scripts/NNModel.py
