##
# @file   NNTop.py
# @author Meng Li
# @date   Jan 2018
# @brief  flow to train/test NN model with tensorflow 
#

import math
import numpy as np

import os
import sys
import glob
import linecache
import commands
import time
import pickle

import Params
import NNModel
from tensorflow.examples.tutorials.mnist import input_data

np.set_printoptions(threshold=np.nan)

def main(params):

    if params.summaryDir == "":
        summaryDir = params.modelDir
    else:
        summaryDir = params.summaryDir

    # parse mnist dataset
    mnistData = input_data.read_data_sets(params.dataDir, one_hot=False)

    # model
    model = NNModel.NNModel(sizeInput=[params.sizeX, params.sizeY, params.sizeZ],
                    layerType=params.layerType,
                    layerShape=params.layerShape,
                    gpuFlag=True if params.gpu >= 0 else False,
                    randomSeed=params.randomSeed,
                    summaryDir=summaryDir)
    modelFile = params.modelFile

    tt = time.time()
    if params.initialModelFile:
        # load initial model
        print("load initial model", params.initialModelFile)
        model.load(params.initialModelFile)

    if params.trainFlag.endswith("Train"):
        # train model
        model.train(trainX=np.reshape(mnistData.train.images, (-1, params.sizeX, params.sizeY, params.sizeZ)),
                    trainY=mnistData.train.labels,
                    testX=np.reshape(mnistData.test.images, (-1, params.sizeX, params.sizeY, params.sizeZ)),
                    testY=mnistData.test.labels,
                    learningRate=0.001,
                    numEpochs=params.numEpochs,
                    batchSize=params.batchSize,
                    noiseAmp=0.0)
        # save model
        if os.path.isdir(params.modelDir) == False:
            os.makedirs(params.modelDir)
        model.save(modelFile)
    else:
        # load model
        model.load(modelFile)

    print("Noise amplitude: %.4f" % params.noiseAmp)

    ### predict
    pred = model.predict(x=np.reshape(mnistData.test.images, (-1, params.sizeX, params.sizeY, params.sizeZ)),
                         noiseAmp=params.noiseAmp,
                         batchSize=params.batchSize)

    model.close()
    print('Kernel takes time(sec): %.4f' % (time.time() - tt))

    print('Accuracy: %.4f' % (np.mean(np.equal(pred, mnistData.test.labels))))

    with open("test.log", 'a') as f:
        f.write("model: " + params.modelFile + " noise amp: " + str(params.noiseAmp) + " accuracy: " + str(np.mean(np.equal(pred, mnistData.test.labels))) + "\n")

    return

if __name__=='__main__':
    params = Params.Params()
    params.load(sys.argv[1])
    main(params)
