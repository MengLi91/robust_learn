## 
# @file scripts/Params.py
# @author Meng Li
# @date   Jan 2018
# Define parameters
#

import json
import math

class Params:
    """
    initilization
    """
    def __init__(self):
        self.dataDir = None # directory for input data
        self.modelDir = None # directory to save model
        self.modelFile = None # model to save/load
        self.initialModelFile = None # initial model to load

        self.sizeOrigX = None # original image width
        self.sizeOrigY = None # original image height
        self.sizeOrigZ = None # original image depth
        self.sizeX = None # clipped image width
        self.sizeY = None # clipped image height
        self.sizeZ = None # clipped image depth

        self.numEpochs = None # number of training epochs
        self.batchSize = None # batch size
        self.trainFlag = None # Predict: 0, Train: 1
        self.randomSeed = None # random seed
        self.noiseAmp = None # amplitude of noise

        self.numSamples = None # total number of samples
        self.numTrainSamples = None # total number of training samples
        self.numTestSamples  = None # total number of testing samples

        self.gpu = 0 # GPU device, -1 if use CPU
        self.summaryDir = None # directory to store summary in tensorflow

        self.layerType = None # array for layer type
        self.layerShape = None # array for layer shape

    """
    convert to json
    """
    def toJson(self):
        data = dict()
        data['dataDir'] = self.dataDir
        data['modelDir'] = self.modelDir
        data['modelFile'] = self.modelFile
        data['initialModelFile'] = self.initialModelFile
        data['sizeOrigX'] = self.sizeOrigX
        data['sizeOrigY'] = self.sizeOrigY
        data['sizeOrigZ'] = self.sizeOrigZ
        data['sizeX'] = self.sizeX
        data['sizeY'] = self.sizeY
        data['sizeZ'] = self.sizeZ
        data['numEpochs'] = self.numEpochs
        data['noiseAmp'] = self.noiseAmp
        data['batchSize'] = self.batchSize
        data['trainFlag'] = self.trainFlag
        data['randomSeed'] = self.randomSeed
        data['numSamples'] = self.numSamples
        data['numTrainSamples'] = self.numTrainSamples
        data['numTestSamples']  = self.numTrainSamples
        data['gpu'] = self.gpu
        data['summaryDir'] = self.summaryDir
        data['layerType'] = self.layerType
        data['layerShape'] = self.layerShape
        return data

    """
    load from json
    """
    def fromJson(self, data):
        if 'dataDir' in data:
            self.dataDir = data['dataDir']
        if 'modelDir' in data:
            self.modelDir = data['modelDir']
        if 'modelFile' in data:
            self.modelFile = data['modelFile']
        if 'initialModelFile' in data:
            self.initialModelFile = data['initialModelFile']
        if 'randomSeed' in data:
            self.randomSeed = data['randomSeed']
        if 'noiseAmp' in data:
            self.noiseAmp = data['noiseAmp']
        if 'sizeOrigX' in data:
            self.sizeOrigX = data['sizeOrigX']
        if 'sizeOrigY' in data:
            self.sizeOrigY = data['sizeOrigY']
        if 'sizeOrigZ' in data:
            self.sizeOrigZ = data['sizeOrigZ']
        if 'sizeX' in data:
            self.sizeX = data['sizeX']
        if 'sizeY' in data:
            self.sizeY = data['sizeY']
        if 'sizeZ' in data:
            self.sizeZ = data['sizeZ']
        if 'numEpochs' in data:
            self.numEpochs = data['numEpochs']
        if 'batchSize' in data:
            self.batchSize = data['batchSize']
        if 'trainFlag' in data:
            self.trainFlag = data['trainFlag']
        if 'numSamples' in data:
            self.numSamples = data['numSamples']
        if 'numTrainSamples' in data:
            self.numTrainSamples = data['numTrainSamples']
        if 'numTestSamples' in data:
            self.numTestSamples = data['numTestSamples']
        if 'gpu' in data:
            self.gpu = data['gpu']
        if 'summaryDir' in data:
            self.summaryDir = data['summaryDir']
        if 'layerType' in data:
            self.layerType = data['layerType']
        if 'layerShape' in data:
            self.layerShape = data['layerShape']

    """
    dump to json file
    """
    def dump(self, filename):
        with open(filename, 'w') as f:
            json.dump(self.toJson(), f)

    """
    load from json file
    """
    def load(self, filename):
        with open(filename, 'r') as f:
            self.fromJson(json.load(f))

    """
    string 
    """
    def __str__(self):
        return str(self.toJson())

    """
    print 
    """
    def __repr__(self):
        return self.__str__()
