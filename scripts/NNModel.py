##
# @file   scripts/NNModel.py
# @author Meng Li
# @date   Jan 2017
# Define NN model
#

import os
import re
import math
import numpy as np
import tensorflow as tf

class NNModel(object):
    """
    initialization
    """
    def __init__(self, sizeInput, layerType, layerShape, gpuFlag=True, randomSeed=10000, summaryDir="summary"):

        # input placeholder
        self.x = tf.placeholder(tf.float32, [None, sizeInput[0], sizeInput[1], sizeInput[2]], name="x")
        # label placeholder
        self.y = tf.placeholder(tf.int32, [None], name="y")
        # trainFlag placeholder
        self.trainFlag = tf.placeholder(tf.bool, [], name="trainFlag")
        # learning rate placeholder 
        self.learningRate = tf.placeholder(tf.float32, [], name='learningRate')
        # noise amplitude plceholder
        self.noiseAmp = tf.placeholder(tf.float32, [], name='noiseAmp')

        # type for each layer
        self.layerType = layerType
        # configuration for each layer
        self.layerShape = layerShape

        # set random seed before build the graph
        tf.set_random_seed(randomSeed)

        # prediction placeholder
        self.logits = self.build(self.x)
        # loss function
        self.loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(labels=self.y, logits=self.logits))
        self.pred = tf.argmax(self.logits, 1)
        self.numCorrectPred = tf.reduce_sum(tf.cast(tf.equal(self.y, tf.cast(self.pred, tf.int32)), tf.int64))
        # running session
        self.session = tf.Session(config=tf.ConfigProto(device_count={'GPU' : int(gpuFlag)}))

        # folder to store summary 
        self.summaryWriterTrain = tf.summary.FileWriter(os.path.join(summaryDir, "train"), self.session.graph)
        self.summaryWriterTest = tf.summary.FileWriter(os.path.join(summaryDir, "test"), self.session.graph)

    def __enter__(self):
        return self
    def __exit__(self, exc_type, exc_value, traceback):
        self.close()

    """
    call this function to destroy globally defined variables in tensorflow
    """
    def close(self): 
        self.summaryWriterTrain.close()
        self.summaryWriterTest.close()
        self.session.close()
        tf.reset_default_graph()

    """
    neural network definition 
    """
    def build(self, x): 
        # create model
        output = x
        for layer in range(len(self.layerType)):
            outputDepth = output.get_shape().as_list()[-1]
            if self.layerType[layer] == "conv":
                convW = self._weight_variable([self.layerShape[layer][0], self.layerShape[layer][1], outputDepth, self.layerShape[layer][2]])
                convb = self._bias_variable([self.layerShape[layer][-1]])
                convW = convW + self.noiseAmp * self._gen_noise(convW)
                convb = convb + self.noiseAmp * self._gen_noise(convb)
                output = tf.nn.relu(self._conv2d(output, convW) + convb)
            elif self.layerType[layer] == "pool":
                output = self._max_pool_2x2(output)
            elif self.layerType[layer] == "reshape":
                outputShape = output.get_shape().as_list()
                output = tf.reshape(output, [-1, outputShape[1] * outputShape[2] * outputShape[3]])
            else:
                fcW = self._weight_variable([outputDepth, self.layerShape[layer][-1]])
                fcb = self._bias_variable([self.layerShape[layer][-1]])
                fcW = fcW + self.noiseAmp * self._gen_noise(fcW)
                fcb = fcb + self.noiseAmp * self._gen_noise(fcb)
                output = tf.matmul(output, fcW) + fcb
                if not layer == len(self.layerType) - 1:
                    output = tf.nn.relu(output)
                    print("adding relu layer")
            print("Layer:", layer, "Shape:", output.get_shape().as_list())
        return output

    """
    training process
    """
    def train(self, trainX, trainY, testX, testY, learningRate=0.01, numEpochs=200, batchSize=128, noiseAmp=0.0, displayStep=None):
        opt = tf.train.AdamOptimizer(learning_rate=self.learningRate)
        optimizer = opt.minimize(self.loss)

        # merge all summaries
        tf.summary.scalar('loss', self.loss)
        mergedSummary = tf.summary.merge_all()

        # Initializing the variables
        self.session.run(tf.variables_initializer(tf.global_variables()))

        # main training iterations
        iteration = 0
        curLearningRate = learningRate
        for epoch in range(numEpochs):
            if epoch == 0:
                print("Epoch %d, training accuracy = %.5f, testing accuracy = %.5f" % (-1, self.validate(trainX, trainY, batchSize=batchSize), self.validate(testX, testY)))

            # permute batches
            perm = np.random.permutation(len(trainX))

            # run on batches
            for batchBegin in range(0, len(trainX), batchSize):
                batchX = trainX[perm[batchBegin:min(batchBegin+batchSize, len(trainX))]]
                batchY = trainY[perm[batchBegin:min(batchBegin+batchSize, len(trainX))]]

                if displayStep and iteration % displayStep == 0:
                    print("Iteration %d, training accuracy = %.6f" % (iteration, self.validate(batchX, batchY)))
                # modify learning rate
                curLearningRate = learningRate if epoch < 100 else learningRate/math.pow(10.0, int((epoch-100)/50)+1)
                feedDict = {
                        self.x: batchX,
                        self.y: batchY,
                        self.trainFlag: True,
                        self.learningRate: curLearningRate,
                        self.noiseAmp: noiseAmp
                        }
                # run optimization
                self.session.run(optimizer, feed_dict=feedDict)
                if iteration == 0:
                    summary = self.session.run(mergedSummary, feed_dict=feedDict)
                    self.summaryWriterTrain.add_summary(summary, epoch)
                iteration += 1

            print("Epoch %d, training accuracy = %.5f, testing accuracy = %.5f, learningRate = %f" % (epoch, self.validate(trainX, trainY, batchSize=batchSize), self.validate(testX, testY), curLearningRate))
        print("Backpropagation finished!")
        return

    def predict(self, x, noiseAmp=0.0, batchSize=128):
        pred = np.zeros(len(x))
        # run on batches
        for batchBegin in range(0, len(x), batchSize):
            # get batch x
            batchX = x[batchBegin:min(batchBegin+batchSize, len(x))]
            pred[batchBegin:min(batchBegin+batchSize, len(x))] = self.session.run(self.pred, feed_dict={self.x: batchX, self.trainFlag: False, self.noiseAmp: noiseAmp})
        return pred

    def validate(self, x, y, noiseAmp=0.0, batchSize=128):
        mergedSummary = tf.summary.merge_all()
        # run on batches
        numCorrectPred = 0
        for batchBegin in range(0, len(x), batchSize):
            # get batch x and y
            batchX = x[batchBegin:min(batchBegin+batchSize, len(x))]
            batchy = y[batchBegin:min(batchBegin+batchSize, len(x))]
            feedDict = {
                    self.x: batchX,
                    self.y: batchy,
                    self.trainFlag: False,
                    self.noiseAmp: noiseAmp}
            numCorrectPred += self.session.run(self.numCorrectPred, feed_dict=feedDict)
        return float(numCorrectPred) / len(x)

    def save(self, filename):
        print("save model", filename)
        saver = tf.train.Saver()
        saver.save(self.session, filename)
        return

    def load(self, filename):
        print("load model", filename)
        saver = tf.train.Saver()
        saver.restore(self.session, filename)

        # restore variables
        graph = tf.get_default_graph()
        self.x = graph.get_tensor_by_name("x:0")
        self.y = graph.get_tensor_by_name("y:0")
        self.trainFlag = graph.get_tensor_by_name("trainFlag:0")
        self.learningRate = graph.get_tensor_by_name("learningRate:0")
        return

    @staticmethod
    def _weight_variable(shape):
        initial = tf.truncated_normal(shape, stddev=0.1)
        return tf.Variable(initial)

    @staticmethod
    def _bias_variable(shape):
        initial = tf.constant(0.1, shape=shape)
        return tf.Variable(initial)

    @staticmethod
    def _conv2d(x, W):
        return tf.nn.conv2d(x, W, strides=[1,1,1,1], padding='SAME')

    @staticmethod
    def _max_pool_2x2(x):
        return tf.nn.max_pool(x, ksize=[1,2,2,1], strides=[1,2,2,1], padding='SAME')

    def _gen_noise(self, x):
        xShape = x.get_shape().as_list()
        weightE = tf.stop_gradient(tf.reduce_mean(tf.abs(x)))
        noise = weightE * tf.truncated_normal(xShape, stddev=1)
        return noise
